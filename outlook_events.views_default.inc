<?php

/**
 * @file
 * Default views file for to create views.
 */

/**
 * Implements hook_views_default_views().
 */
function outlook_events_views_default_views() {
  $view = new view();
  $view->name = 'outlook_events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Outlook events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Outlook events';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer outlook events';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'outlook_owner',
      'rendered' => 0,
      'rendered_strip' => 1,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'event_organizer' => 'event_organizer',
    'event_location' => 'event_location',
    'outlook_events_date' => 'outlook_events_date',
    'outlook_events_date_1' => 'outlook_events_date_1',
    'outlook_owner' => 'outlook_owner',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'event_organizer' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'event_location' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'outlook_events_date' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'outlook_events_date_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'outlook_owner' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Organizer */
  $handler->display->display_options['fields']['event_organizer']['id'] = 'event_organizer';
  $handler->display->display_options['fields']['event_organizer']['table'] = 'field_data_event_organizer';
  $handler->display->display_options['fields']['event_organizer']['field'] = 'event_organizer';
  /* Field: Content: Location */
  $handler->display->display_options['fields']['event_location']['id'] = 'event_location';
  $handler->display->display_options['fields']['event_location']['table'] = 'field_data_event_location';
  $handler->display->display_options['fields']['event_location']['field'] = 'event_location';
  /* Field: Content: Date */
  $handler->display->display_options['fields']['outlook_events_date']['id'] = 'outlook_events_date';
  $handler->display->display_options['fields']['outlook_events_date']['table'] = 'field_data_outlook_events_date';
  $handler->display->display_options['fields']['outlook_events_date']['field'] = 'outlook_events_date';
  $handler->display->display_options['fields']['outlook_events_date']['label'] = 'Start Date';
  $handler->display->display_options['fields']['outlook_events_date']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'value',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Date */
  $handler->display->display_options['fields']['outlook_events_date_1']['id'] = 'outlook_events_date_1';
  $handler->display->display_options['fields']['outlook_events_date_1']['table'] = 'field_data_outlook_events_date';
  $handler->display->display_options['fields']['outlook_events_date_1']['field'] = 'outlook_events_date';
  $handler->display->display_options['fields']['outlook_events_date_1']['label'] = 'End Date';
  $handler->display->display_options['fields']['outlook_events_date_1']['settings'] = array(
    'format_type' => 'medium',
    'fromto' => 'value2',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Outlook calendar owner */
  $handler->display->display_options['fields']['outlook_owner']['id'] = 'outlook_owner';
  $handler->display->display_options['fields']['outlook_owner']['table'] = 'field_data_outlook_owner';
  $handler->display->display_options['fields']['outlook_owner']['field'] = 'outlook_owner';
  $handler->display->display_options['fields']['outlook_owner']['label'] = 'Events of';
  $handler->display->display_options['fields']['outlook_owner']['exclude'] = TRUE;
  $handler->display->display_options['fields']['outlook_owner']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Date -  start date (outlook_events_date) */
  $handler->display->display_options['sorts']['outlook_events_date_value']['id'] = 'outlook_events_date_value';
  $handler->display->display_options['sorts']['outlook_events_date_value']['table'] = 'field_data_outlook_events_date';
  $handler->display->display_options['sorts']['outlook_events_date_value']['field'] = 'outlook_events_date_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'outlook_events' => 'outlook_events',
  );
  /* Filter criterion: Content: Date -  start date (outlook_events_date) */
  $handler->display->display_options['filters']['outlook_events_date_value']['id'] = 'outlook_events_date_value';
  $handler->display->display_options['filters']['outlook_events_date_value']['table'] = 'field_data_outlook_events_date';
  $handler->display->display_options['filters']['outlook_events_date_value']['field'] = 'outlook_events_date_value';
  $handler->display->display_options['filters']['outlook_events_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['outlook_events_date_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['outlook_events_date_value']['expose']['operator_id'] = 'outlook_events_date_value_op';
  $handler->display->display_options['filters']['outlook_events_date_value']['expose']['label'] = 'Events starting from';
  $handler->display->display_options['filters']['outlook_events_date_value']['expose']['operator'] = 'outlook_events_date_value_op';
  $handler->display->display_options['filters']['outlook_events_date_value']['expose']['identifier'] = 'outlook_events_date_value';
  $handler->display->display_options['filters']['outlook_events_date_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['outlook_events_date_value']['form_type'] = 'date_popup';
  /* Filter criterion: Content: Date - end date (outlook_events_date:value2) */
  $handler->display->display_options['filters']['outlook_events_date_value2']['id'] = 'outlook_events_date_value2';
  $handler->display->display_options['filters']['outlook_events_date_value2']['table'] = 'field_data_outlook_events_date';
  $handler->display->display_options['filters']['outlook_events_date_value2']['field'] = 'outlook_events_date_value2';
  $handler->display->display_options['filters']['outlook_events_date_value2']['operator'] = '<=';
  $handler->display->display_options['filters']['outlook_events_date_value2']['exposed'] = TRUE;
  $handler->display->display_options['filters']['outlook_events_date_value2']['expose']['operator_id'] = 'outlook_events_date_value2_op';
  $handler->display->display_options['filters']['outlook_events_date_value2']['expose']['label'] = 'Events ends within';
  $handler->display->display_options['filters']['outlook_events_date_value2']['expose']['operator'] = 'outlook_events_date_value2_op';
  $handler->display->display_options['filters']['outlook_events_date_value2']['expose']['identifier'] = 'outlook_events_date_value2';
  $handler->display->display_options['filters']['outlook_events_date_value2']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['outlook_events_date_value2']['form_type'] = 'date_popup';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'outlook-events';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['block_description'] = 'Outlook Events';

  $views[$view->name] = $view;
  return $views;
}
