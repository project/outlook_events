<?php
/**
 * @file
 * Provides admin form for outlook events.
 */

/**
 * Menu page callback: builds the page for administering shortcut sets.
 */
function outlook_events_account_admin() {
  $outlook_accounts = outlook_events_account();
  $header = array(t('Name'), t('Mail'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  $rows = array();
  if (!empty($outlook_accounts)) {
    foreach ($outlook_accounts as $account) {
      $row = array(
        check_plain($account->name),
        check_plain($account->mail),
        l(t('edit account'), "admin/config/people/outlook-account/$account->id/edit"),
        l(t('delete'), "admin/config/people/outlook-account/$account->id/delete"),
      );
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(t('No accounts has been added yet.'), '', '');
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Form callback: builds the form for adding a outlook account.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @param $form_state
 *   An associative array containing the current state of the form.
 *
 * @return array
 *   An array representing the form definition.
 *
 * @ingroup forms
 *
 * @see outlook_account_add_form_validate()
 * @see outlook_account_add_form_submit()
 */
function outlook_events_account_add_form($form, &$form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account name'),
    '#description' => t('Name of the account.'),
    '#required' => TRUE,
  );
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Exchange MailID'),
    '#description' => t('The Exchange MailID to retrive events from.'),
    '#required' => TRUE,
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Password for the above mentioned EmailID.'),
    '#required' => TRUE,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add account'),
  );

  return $form;
}

/**
 * Validation handler for outlook_events_account_add_form().
 */
function outlook_events_account_add_form_validate($form, &$form_state) {
  $account = new stdClass();
  $account->mail = $form_state['values']['mail'];
  $account->pass = $form_state['values']['password'];
  // Check to prevent a duplicate mail.
  if (outlook_events_mail_exists($form_state['values']['mail'])) {
    form_set_error('mail', t('The MailID %mail already exists.', array('%mail' => $form_state['values']['mail'])));
  }
  elseif (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t('The MailID %mail is not valid.', array('%mail' => $form_state['values']['mail'])));
  }
  elseif (!outlook_events_list($account, TRUE)) {
    form_set_error('mail', t('The MailID %mail is not an exchange account or else you have provided incorrect credentials.', array('%mail' => $form_state['values']['mail'])));
  }
}

/**
 * Submit handler for outlook_events_account_add_form().
 */
function outlook_events_account_add_form_submit($form, &$form_state) {
  // Save a new outlook account.
  db_insert('outlook_events_account')
    ->fields(array(
      'name' => $form_state['values']['name'],
      'mail' => $form_state['values']['mail'],
      'pass' => $form_state['values']['password'],
    ))
    ->execute();
  drupal_set_message(t('The %name outlook account has been added. You can edit it from this page.', array('%name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'admin/config/people/outlook-account';
}

/**
 * Form callback: builds the form for editing a outlook account.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @param $form_state
 *   An associative array containing the current state of the form.
 *
 * @return array
 *   An array representing the form definition.
 *
 * @ingroup forms
 *
 * @see outlook_events_account_edit_form_validate()
 * @see outlook_events_account_edit_form_submit()
 */
function outlook_events_account_edit_form($form, &$form_state, $id) {
  $account = outlook_events_account_by_id($id);
  $form_state['account_id'] = $id;
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account name'),
    '#description' => t('Name of the account.'),
    '#default_value' => $account['0']->name,
    '#required' => TRUE,
  );
  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Exchange MailID'),
    '#default_value' => $account['0']->mail,
    '#description' => t('The Exchange MailID to retrive events from.'),
    '#required' => TRUE,
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Edit to change the current password.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update account details'),
  );

  return $form;
}

/**
 * Validation handler for outlook_events_account_edit_form().
 */
function outlook_events_account_edit_form_validate($form, &$form_state) {
  $act_account = new stdClass();
  $act_account->success = FALSE;
  $act_account->mail = $form_state['values']['mail'];
  $act_account->pass = $form_state['values']['password'];
  $account = outlook_events_account_by_id($form_state['account_id']);
  // Check to prevent a duplicate mail.
  if ($account['0']->mail != $form_state['values']['mail']) {
    if (outlook_events_mail_exists($form_state['values']['mail'])) {
      form_set_error('mail', t('The MailID %mail already exists.', array('%mail' => $form_state['values']['mail'])));
    }
    elseif (!valid_email_address($form_state['values']['mail'])) {
      form_set_error('mail', t('The MailID %mail is not valid.', array('%mail' => $form_state['values']['mail'])));
    }
    elseif (!outlook_events_list($act_account, TRUE)) {
      form_set_error('mail', t('The MailID %mail is not an exchange account or else you have provided incorrect credentials.', array('%mail' => $form_state['values']['mail'])));
    }
  }
  if (!empty($form_state['values']['password']) && !outlook_events_list($act_account, TRUE)) {
    form_set_error('password', t('The MailID %mail is not an exchange account or else you have provided incorrect credentials.', array('%mail' => $form_state['values']['mail'])));
  }
}

/**
 * Submit handler for outlook_events_account_edit_form().
 */
function outlook_events_account_edit_form_submit($form, &$form_state) {
  $account = outlook_events_account_by_id($form_state['account_id']);
  if (empty($form_state['values']['password'])) {
    $password = $account['0']->pass;
  }
  else {
    $password = $form_state['values']['password'];
  }
  // Update outlook account details.
  db_update('outlook_events_account')
    ->condition('id', $form_state['account_id'])
    ->fields(array(
      'name' => $form_state['values']['name'],
      'mail' => $form_state['values']['mail'],
      'pass' => $password,
    ))
    ->execute();
  drupal_set_message(t('The %name outlook account has been updated.', array('%name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'admin/config/people/outlook-account';
}

/**
 * Form callback: builds the form for deleting a outlook account.
 */
function outlook_events_account_delete_form($form, &$form_state, $acc_id) {
  $form = array();
  $form['acc_id'] = array(
    '#type' => 'value',
    '#value' => $acc_id,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete this account?'),
    'admin/config/people/outlook-account',
    t('This action cannot be undone.'),
    t('Delete account'),
    t('Cancel')
  );
}

/**
 * Submit handler for the outlook_events_account_delete_form.
 */
function outlook_events_account_delete_form_submit($form, &$form_state) {
  if (isset($form_state['values']['acc_id'])) {
    db_delete('outlook_events_account')
      ->condition('id', $form_state['values']['acc_id'])
      ->execute();
  }
  drupal_set_message(t('You have successfully deleted the account.'));
  $form_state['redirect'] = 'admin/config/people/outlook-account';
}
