CONTENTS OF THIS FILE
---------------------

 * About Outlook events
 * Requirements
 * Installation
 * Configuration

ABOUT OUTLOOK EVENTS
------------

Outlook events module fetches calendar events from exchange outlook account and
creates nodes in the drupal end.


REQUIREMENTS
------------

This module requires date, date_popup, views and libraries module.

You should download https://github.com/jamesiarmes/php-ews library and it put
under libraries folder.  So that all files inside it are listed under
sites/all/libraries/php-ews.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has an interface to add accounts admin/config/people/outlook-account

USAGE
-----

1. Add your exchange outlook accounts in admin/config/people/outlook-account

2. Run the cron.

3. You could see all the events listed in outlook-events page!!

4. A block named Outlook events is also available in blocks page.
